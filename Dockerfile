#Download base image ubuntu 16.04
FROM ubuntu:16.04

# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

#Update Ubuntu Repo, Install Python
RUN apt-get update && apt-get install -y \
    python-pip \
    python-dev \
    curl \
    build-essential \
    python-dev \
    python-pip \
    libxml2-dev \
    libxslt1-dev \
    zlib1g-dev \
    libffi-dev \
    libssl-dev \
    git

#update pip
RUN pip install -U pip

# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 8.0.0

# install nvm
# https://github.com/creationix/nvm#install-script
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash

# install node and npm
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

#Install scrapy + scrapoxy + botocore
RUN pip install scrapy && npm install -g scrapoxy
RUN pip install scrapy scrapoxy && pip install botocore

#Clone private repo
RUN git clone https://ArturWagner:f07d68ec37db@bitbucket.org/ArturWagner/consultacnpj.git
RUN mkdir /usr/pasta-cnpj && touch /usr/pasta-cnpj/estado.csv
#change workdir and add env nome_out
WORKDIR consultacnpj
ENV nome default

#Start Crawler + Start Scrapoxy
ENTRYPOINT ./entrypoint.sh
