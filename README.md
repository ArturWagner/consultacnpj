# consultacnpj

## Running local
### install scrapy
1. Upgrade pip: `pip install -U pip `
2. Install Scrapy: `pip install scrapy`
3. Full installation guide [here](https://doc.scrapy.org/en/latest/intro/install.html)
### install scrapoxy
1. Desired Node version : `8.0.0`
2. Install Scrapoxy: `sudo npm install -g scrapoxy`
3. Full installation guide [here](http://scrapoxy.readthedocs.io/en/master/quick_start/index.html)
### init scrapoxy
1. Go to your project folder and type: `scrapoxy start conf.json -d`
2. Check if Scrapoxy is reachable at: http://localhost:8888
### init scrapy crawler
1. Set the csv file with cnpj's list on `/usr/pasta-cnpj/listas_cnpj.csv`
2. Change AWS credentials and bucket adress on `setting.py`
3. Go to your project folder and type: `scrapy crawl consulta`
4. If log is desired use nohup: `nohup scrapy crawl consulta &> crawler.out&`
... Note: The data collected with the crawler goes to AWS bucket.
## Running from docker
1. Set the git clone url to this project on docker file
2. Build docker image: `docker build -t consultacnpj .`
... Dont forget the . !!
3. Run the container:
```
docker run  -e 'nome=output_csv_name' -v /path/to/listas_cnpj/:/usr/pasta-cnpj/ -t consultacnpj
```
> The cnpj list must always be named lista_cnpj.csv
