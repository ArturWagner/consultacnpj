# -*- coding: utf-8 -*-
import scrapy
import json
import csv
#pronto pra rodar

class ConsultaSpider(scrapy.Spider):
    name = 'consulta'
    allowed_domains = ['receitaws.com.br']
    with open('/usr/pasta-cnpj/listas_cnpj.csv') as csvfile:
        reader = csv.reader(csvfile)
        cnpjs = list(reader)
        row_count = len(cnpjs)
        cem = cnpjs[0:999999] #se quiser restringir numeros de request

    urls = []

    for cnpj in cem:
        urls.append('https://receitaws.com.br/v1/cnpj/%s?callback=' % str(cnpj).strip("['']"))

    start_urls = urls

    def parse(self, response):
        cartao = json.loads(response.body_as_unicode())
        yield {
            'status_resposta': cartao['status'],
            'cnpj': cartao['cnpj'],
            'tipo': cartao['tipo'],
            'abertura': cartao['abertura'],
            'nome': cartao['nome'],
            'fantasia': cartao['fantasia'],
            'atividade_principal_text': cartao['atividade_principal'][0]['text'],
            'atividade_principal_cnae': cartao['atividade_principal'][0]['code'],
            'atividades_secundarias_text': cartao['atividades_secundarias'][0]['text'],
            'atividades_secundarias_cnae': cartao['atividades_secundarias'][0]['code'],
            'natureza_juridica': cartao['natureza_juridica'],
            'logradouro': cartao['logradouro'],
            'numero': cartao['numero'],
            'complemento': cartao['complemento'],
            'cep': cartao['cep'],
            'bairro': cartao['bairro'],
            'municipio': cartao['municipio'],
            'uf': cartao['uf'],
            'email': cartao['email'],
            'telefone': cartao['telefone'],
            'efr': cartao['efr'],
            'situacao': cartao['situacao'],
            'data_situacao': cartao['data_situacao'],
            'motivo_situacao': cartao['motivo_situacao'],
            'situacao_especial': cartao['situacao_especial'],
            'data_situacao_especial': cartao['data_situacao_especial'],
            'capital_social': cartao['capital_social'],
            'qsa': cartao['qsa'],
            'extra': cartao['extra']
        }
